package com.epam.engx.cleancode.naming.task5.service;


import com.epam.engx.cleancode.naming.task5.thirdpartyjar.CustomerContact;

public interface CustomerContactService {

    CustomerContact findCustomerContactDetailsByCustomerId(Long customerId);

    void updateCustomerContactDetails(CustomerContact customerContactDetails);

}
