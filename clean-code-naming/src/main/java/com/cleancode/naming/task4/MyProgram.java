package com.epam.engx.cleancode.naming.task4;

import java.io.IOException;

public class MyProgram {

	public static void main(String[] args) throws IOException {
		MyImage My_Image = MyImage.s("epam.png");
		for (int o = 0; o < My_Image.h(); o += 4) {  // print image
			for (int i = 0; i < My_Image.w(); i += 2) {
				// print pixel
				if (My_Image.r(i, o) + My_Image.b(i, o) + My_Image.g(i, o) > 600) {
					System.out.print(" ");
				} else if (My_Image.b(i, o) > 200) {
					System.out.print("<");
				} else {
					System.out.print("#");
				}
			}
			System.out.println();
		}
	}

}
