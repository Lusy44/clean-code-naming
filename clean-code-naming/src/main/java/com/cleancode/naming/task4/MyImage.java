package com.epam.engx.cleancode.naming.task4;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class MyImage {

	private BufferedImage i;

	public static MyImage s(String string) {
		return new MyImage(string);
	}

	public int h() {
		return i.getHeight();
	}

	public int w() {
		return i.getWidth();
	}

	public int r(int x, int y) {
		int dValue = inner(x, y);
		return (dValue >> 16) & 0xFF;
	}

	public int g(int x, int y) {
		int cValye = inner(x, y);
		return (cValye >> 8) & 0xFF;
	}

	public int b(int x, int y) {
		int iValue = inner(x, y);
		return iValue & 0xFF;
	}

	private int inner(int a, int b) {
		if (a < 0 || a > i.getWidth()) {
			throw new RuntimeException("Coordinate x out of range: 0.." + i.getWidth());
		} else if (b < 0 || b > i.getHeight()) {
			throw new RuntimeException("Coordinate y out of range: 0.." + i.getHeight());
		}
		int bValue = i.getRGB(a, b);
		return bValue;
	}

	private MyImage(String string) {
		this.i = myFile(string);
	}

	private BufferedImage myFile(String f) {
		try {
			return ImageIO.read(getClass().getClassLoader().getResource(f));
		} catch (IOException e) {
			throw new RuntimeException("File not found!", e);
		}
	}

}
