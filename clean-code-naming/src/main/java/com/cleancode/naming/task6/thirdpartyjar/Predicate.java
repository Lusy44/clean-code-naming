package com.epam.engx.cleancode.naming.task6.thirdpartyjar;

public interface Predicate<T> {

    boolean test(T fileName);
}
